// easystar path finding init
var easystar = new EasyStar.js();
easystar.setGrid(gridMap);
easystar.setAcceptableTiles([0]);
easystar.enableDiagonals();
easystar.enableCornerCutting();

// function using for checking if GameObject reach the end
function ifReached(currentXTile, currentYTile, endXTile, endYTile)
{
    if (currentXTile === endXTile && currentYTile === endYTile)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// start point x, y || end point x, y
function enemyFindPath(currentXTile, currentYTile, endXTile, endYTile, GameObject)
{
    easystar.calculate();
    // console.log("enemyfindpath loaded");
    easystar.findPath(currentXTile, currentYTile, endXTile, endYTile, function(path)
    {
        if (path === null) 
        {
            console.log("The path to the destination point was not found.");
            gridMapClear();
        }
        else if (path && !ifReached(currentXTile, currentYTile, endXTile, endYTile)) 
        {
            var currentNextPointX = path[1].x;
            var currentNextPointY = path[1].y;
        }

        if (currentNextPointX < currentXTile && currentNextPointY < currentYTile) 
        {// left up          
            GameObject.gamerInput = new GamerInput("Left Up");
        }
        else if (currentNextPointX == currentXTile && currentNextPointY < currentYTile)
        {// up                
            GameObject.gamerInput = new GamerInput("Up");
        }
        else if (currentNextPointX > currentXTile && currentNextPointY < currentYTile)
        {// right up
            GameObject.gamerInput = new GamerInput("Right Up");
        }
        else if (currentNextPointX < currentXTile && currentNextPointY == currentYTile)
        {// left
            GameObject.gamerInput = new GamerInput("Left");
        }
        else if (currentNextPointX > currentXTile && currentNextPointY == currentYTile)
        {// right    
            GameObject.gamerInput = new GamerInput("Right");
        }
        else if (currentNextPointX > currentXTile && currentNextPointY > currentYTile)
        {// right down  
            GameObject.gamerInput = new GamerInput("Right Down");
        }
        else if (currentNextPointX == currentXTile && currentNextPointY > currentYTile)
        {// down
            GameObject.gamerInput = new GamerInput("Down");
        }
        else if (currentNextPointX < currentXTile && currentNextPointY > currentYTile)
        {// left down
            GameObject.gamerInput = new GamerInput("Left Down");
        }
        else
        {            
            GameObject.gamerInput = new GamerInput("None");
        }
    });
}
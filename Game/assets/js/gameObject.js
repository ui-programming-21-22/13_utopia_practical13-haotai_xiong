// grid map init
// 20 x 10
// value means this grid is a tower(3 different towers) or wall or nothing(road)
let gridMap = [ [0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0]];
// background image
let m_backgroundImage = new Image();
m_backgroundImage.src = "assets/image/dark_blue_background.png";
// grid attributes
// const s_gridSize = 20;
const s_gridWidth = 50;
const s_gridHeight = 50;
const s_gridMapWidthNum = canvas.width / s_gridWidth;
const s_gridMapHeightNum = canvas.height / s_gridHeight - 1;
// mouse current x,y grid attribute
let m_mouseCurrentXTile = 0;
let m_mouseCurrentYTile = 0;
// wall image setup
const m_wallScale = 1;
const m_wallWidth = s_gridWidth;
const m_wallHeight = s_gridHeight;
let m_wallImage = new Image();
m_wallImage.src = "assets/image/wall.png";
let m_wallToolbarImage = new Image();
m_wallToolbarImage.src = "assets/image/wall_toolbar.png";
// player setup
const m_playerScale = 1;
const m_playerWidth = 50;
const m_playerHeight = 50;
const m_playerScaledWidth = m_playerScale * m_playerWidth;
const m_playerScaledHeight = m_playerScale * m_playerHeight;
let m_playerImage = new Image();
m_playerImage.src = "assets/image/Bioshock_edited.png";
// bersek tower setup
const m_bersekTowerScale = 1;
const m_bersekTowerWidth = 50;
const m_bersekTowerHeight = 50;
const m_bersekTowerScaledWidth = m_bersekTowerScale * m_bersekTowerWidth;
const m_bersekTowerScaledHeight = m_bersekTowerScale * m_bersekTowerHeight;
let m_bersekTowerImage = new Image();
m_bersekTowerImage.src = "assets/image/Bersek-Idle&Attack&Bullet.png";
let m_bersekTowerBulletImage = new Image();
m_bersekTowerBulletImage.src = "assets/image/Bersek-Idle&Attack&Bullet.png";
// zombie enemy setup
const m_zombieScale = 1;
const m_zombieWidth = 50;
const m_zombieHeight = 50;
const m_zombieScaledWidth = m_zombieScale * m_zombieWidth;
const m_zombieScaledHeight = m_zombieScale * m_zombieHeight;
let m_zombieImage = new Image();
m_zombieImage.src = "assets/image/zombie-sprite-sheet.png";
// health bar
var healthBarWidth = 50;
var healthBarHeight = 20;
var healthBarMax = 100;
var healthBarVal = 0;
// money
var money = 0;
let m_moneyTimeCounter = 0;
let m_zombieRespawnTimeCounter = 0;
// arrays
let m_zombieArray = [];
let m_zombieRespawn = [];
let m_bersekArray = [];

// input function
function GamerInput(input) 
{
    this.action = input;
}

// bulletObject init
function bulletObject(sprite, xpos, ypos, width, height, canDraw, timeCounter)
{
    this.sprite = sprite;
    this.xpos = xpos;
    this.ypos = ypos;
    this.width = width;
    this.height = height;
    this.canDraw = canDraw;
    this.timeCounter = timeCounter;
}

// GameObject init
function GameObject(sprite, attribute, xpos, ypos, width, height, speed,
    currentXTile, currentYTile, gamerInput,
    walkLoop, frameLimit, frameCount, currentLoopIndex, currentDirection,
    bullet, health)
{
    this.sprite = sprite;
    this.attribute = attribute;
    this.xpos = xpos;
    this.ypos = ypos;
    this.width = width;
    this.height = height;
    this.speed = speed;
    this.currentXTile = currentXTile;
    this.currentYTile = currentYTile;
    this.gamerInput = gamerInput;
    this.walkLoop = walkLoop;
    this.frameLimit = frameLimit;
    this.frameCount = frameCount;
    this.currentLoopIndex = currentLoopIndex;
    this.currentDirection = currentDirection;
    this.bullet = bullet;
    this.health = health;
}

// bullet init
// bersek bullet
// let m_bersekBullet = new bulletObject(m_bersekTowerBulletImage, 0, 0, 50, 50, false, 0);

let player = new GameObject(m_playerImage, "player", 950, 450, m_playerScaledWidth, m_playerScaledHeight, 2,
	2, 2, new GamerInput("None"),
	[0, 1, 0, 2], 7, 0, 0, 0, "None", 0);

// zombie array init
function zombieSpawn()
{
    m_zombieRespawnTimeCounter++;
    if (m_zombieRespawnTimeCounter > zombieRespawnTimeLimit
        && m_zombieArray.length < 5)
    {
        m_zombieRespawnTimeCounter = 0;
        // default zombie
        let newZombie = new GameObject(m_zombieImage, "zombieEnemy", 0, 0, m_zombieScaledWidth, m_zombieScaledHeight, 0.2,
            0, 0, new GamerInput("None"),
            [1, 0, 1, 2], 7, 0, 0, 0, "None", 100);
        m_zombieArray.push(newZombie);
        m_zombieRespawn.push(true);
    }
}

function checkScoreDetector()
{
    //m_localStorageChecked = false;
    if (!m_localStorageChecked)
    {
        moneyDetector = localStorage.getItem('money');
        mapDetector = localStorage.getItem('gridMap');
        zombieDetector = localStorage.getItem('zombie');
        // money detector
        if (moneyDetector)
        {
            console.log("money local storage detected");
            money = parseInt(moneyDetector);
        }
        else
        {
            console.log("money local storage not detected");
            money = 0;
        }
        // map detector
        if (mapDetector)
        {
            //gridMap = mapDetector;
        }
        // zombie detector
        if (zombieDetector)
        {
            //m_zombieArray = zombieDetector;
        }
        m_localStorageChecked = true;
    }
}

function tileUpdate(GameObject)
{
    var testCurrentXTile = Math.floor(GameObject.xpos/ s_gridWidth);
    if (Math.round(GameObject.xpos) === testCurrentXTile * s_gridWidth)
    {
        GameObject.currentXTile = testCurrentXTile;
    }
    var testCurrentYTile = Math.floor(GameObject.ypos / s_gridHeight);
    if (Math.round(GameObject.ypos) === testCurrentYTile * s_gridHeight)
    {
        GameObject.currentYTile = testCurrentYTile;
    }
}

function mouseTileUpdate(event)
{
    m_mouseCurrentXTile = Math.floor(event.offsetX / s_gridWidth);
    m_mouseCurrentYTile = Math.floor(event.offsetY / s_gridHeight);
    //console.log("mouse tile updating, x: " + m_mouseCurrentXTile + ", y: " + m_mouseCurrentYTile);
}

// draw randPos Brain
function generateRandPos(maxX, maxY, delta)
{
    this.x = Math.abs(Math.floor(Math.random() * maxX) + delta);
    this.y = Math.abs(Math.floor(Math.random() * maxY) + delta);
}

// reset whole map
function gridMapClear()
{
    for (let i = 0; i < s_gridMapHeightNum; i++)
    {
        var tile = gridMap[i];
        for (let j = 0; j < gridMap[i].length; j++)
        {
            tile[j] = 0;
        }
    }
}
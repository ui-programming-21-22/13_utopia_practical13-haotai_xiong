// function import

// score
let username = localStorage.getItem('username');
let header = document.getElementById("main-header");

if (username)
{
    header.innerHTML = "Hello " + username;
}


// nipplejs setup
/*
var dynamic = nipplejs.create({
    zone: document.getElementById('joystick'),
    color: 'grey',
});

dynamic.on('added', function (evt, nipple){
    nipple.on('dir:up', function (evt, data){
        gamerInput = new GamerInput("Up");
    });
    nipple.on('dir:down', function (evt, data){
        gamerInput = new GamerInput("Down");
    });
    nipple.on('dir:left', function (evt, data){
        gamerInput = new GamerInput("Left");
    });
    nipple.on('dir:right', function (evt, data){
        gamerInput = new GamerInput("Right");
    });
    nipple.on('end', function (evt, data){
        gamerInput = new GamerInput("None");
    });
});
*/


function gameloop(event) 
{
    setInterval(update(event), 2000);
    draw();
    window.requestAnimationFrame(gameloop);
}

// key event
window.addEventListener('keydown', input);
window.addEventListener('keyup', input);
window.addEventListener('keydown', setLocalStorage);
// mouse
window.addEventListener('mousedown', mouseTileUpdate);
window.addEventListener('mouseup', mouseTileUpdate);
window.addEventListener('mousedown', input);
window.addEventListener('mouseup', input);
window.addEventListener('mousedown', setLocalStorage);
//window.addEventListener('mouseup', setLocalStorage);
// touch
window.addEventListener('touchstart', mouseTileUpdate);
window.addEventListener('touchend', mouseTileUpdate);
window.addEventListener('touchstart', input);
window.addEventListener('touchend', input);
//window.addEventListener('mousedown', getMousePos);
//window.addEventListener('mouseup', getMousePos);

window.requestAnimationFrame(gameloop);
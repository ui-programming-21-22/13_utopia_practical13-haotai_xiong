function drawGrid()
{
    for (let i = 0; i < s_gridMapHeightNum; i++)
    {
        var tile = gridMap[i];
        for (let j = 0; j < gridMap[i].length; j++)
        {
            switch(tile[j])
            {
                case 0:
                    break;
                case 1:
                    context.drawImage(m_wallImage, j * m_wallWidth, i * m_wallHeight)
                    break;
                default:
                    break;
            }
        }
    }
}

// Draw a HealthBar on Canvas, can be used to indicate players health
function drawHealthbar(GameObject)
{
    // Draw the background
    context.fillStyle = "#000000";
    // context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillRect(GameObject.xpos - ((healthBarWidth - GameObject.width) / 2.0), GameObject.ypos - healthBarHeight, healthBarWidth, healthBarHeight);

    // Draw the fill
    context.fillStyle = "#00FF00";
    var fillVal = Math.min(Math.max(GameObject.health / healthBarMax, 0), 1);
    context.fillRect(GameObject.xpos - ((healthBarWidth - GameObject.width) / 2.0), GameObject.ypos - healthBarHeight, fillVal * healthBarWidth, healthBarHeight);
}

function drawFrame(image, frameX, frameY, canvasX, canvasY, GameObject)
{
    context.drawImage(image, frameX * GameObject.width, frameY * GameObject.height, GameObject.width, GameObject.height,
        canvasX, canvasY, GameObject.width, GameObject.height);
}

function animate(GameObject)
{
    if (GameObject.gamerInput.action != "None") 
    {
        GameObject.frameCount++;
        //console.log(currentLoopIndex);
        if (GameObject.frameCount >= GameObject.frameLimit) {
            GameObject.frameCount = 0;
            GameObject.currentLoopIndex++;
            if (GameObject.currentLoopIndex == GameObject.walkLoop.length) {
                GameObject.currentLoopIndex = 0;
            }
        }
    }
    else 
    {
        GameObject.currentLoopIndex = 0;
    }
    drawFrame(GameObject.sprite, GameObject.walkLoop[GameObject.currentLoopIndex], GameObject.currentDirection,
    GameObject.xpos, GameObject.ypos, GameObject);
}

function moneyCounter()
{
    context.font = "bold 15px solid";
    var scoreCounterString = "Money: " + money;
    context.fillStyle = "#000000";
    context.fillText(scoreCounterString, 0, canvas.height);
}

function drawBersekTower()
{
    for (var i = 0; i < m_bersekArray.length; i++)
    {
        animate(m_bersekArray[i]);
        // draw bullet
        if (m_bersekArray[i].bullet.canDraw)
        {
            //console.log("draw bullet");
            context.drawImage(m_bersekArray[i].bullet.sprite, 0, 200, 50, 50,
                m_bersekArray[i].bullet.xpos, m_bersekArray[i].bullet.ypos, 50, 50);
            m_bersekArray[i].bullet.timeCounter += 1;
            if (m_bersekArray[i].bullet.timeCounter > timeLimit)
            {
                m_bersekArray[i].bullet.timeCounter = 0;
                m_bersekArray[i].bullet.canDraw = false;
            }
        }
    }
}

function drawZombieArray()
{ 
    for (let i = 0; i < m_zombieArray.length; i++)
    {
        if (m_zombieRespawn[i])
        {
            animate(m_zombieArray[i]);
            drawHealthbar(m_zombieArray[i]);
        }
    }
}

function drawBackground()
{
    context.drawImage(m_backgroundImage, 0, 0);
}

function draw()
{
    context.clearRect(0,0, canvas.width, canvas.height);
    //drawBackground();
    animate(player);
    drawBersekTower();
    moneyCounter();
    setInterval(drawGrid(), 2000);
    drawToolbar();
    drawZombieArray();
    //----------------
}
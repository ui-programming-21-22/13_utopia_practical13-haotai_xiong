// toolBar attributes
m_toolbarItemWidth = 50;
m_toolbarItemHeight = 50;
// toolbar item function
function toolbarObject(sprite, xpos, ypos, width, height,
    currentXTile, currentYTile, attribute, isChoosen)
{
    this.sprite = sprite;
    this.xpos = xpos;
    this.ypos = ypos;
    this.width = width;
    this.height = height;
    this.currentXTile = currentXTile;
    this.currentYTile = currentYTile;
    this.attribute = attribute;
    this.isChoosen = isChoosen;
}
// toolbar item init
let m_wallToolbar = new toolbarObject(m_wallToolbarImage, 156, 506, 50, 50,
    2, 10, "wall", false);
let m_bersekToolbar = new toolbarObject(m_bersekTowerImage, 250, 500, 50, 50,
    3, 10, "bersek", false);

// function for drawing toolbar
function drawToolbar()
{
    // wall
    context.drawImage(m_wallToolbar.sprite, 0, 0, m_toolbarItemWidth, m_toolbarItemHeight,
        m_wallToolbar.xpos, m_wallToolbar.ypos, m_wallToolbar.width, m_wallToolbar.height);
    context.font = '20px serif';
    context.fillText("$10", 100, 550, 50);
    // bersek tower
    context.drawImage(m_bersekToolbar.sprite, 0, 100, m_toolbarItemWidth, m_toolbarItemHeight, 
        m_bersekToolbar.xpos, m_bersekToolbar.ypos, m_bersekToolbar.width, m_bersekToolbar.height);
    context.fillText("$100", 200, 550, 50);
}

// function for creating the tower
function create(toolbarObject)
{
    window.requestAnimationFrame(mouseTileUpdate);
    if (toolbarObject.isChoosen 
        && m_mouseCurrentYTile < 10
        && gridMap[m_mouseCurrentYTile][m_mouseCurrentXTile] === 0)
    {
        switch (toolbarObject.attribute)
        {
            case "wall":
                if (money >= 10)
                {
                    console.log("wall created");
                    gridMap[m_mouseCurrentYTile][m_mouseCurrentXTile] = 1;
                    money -= 10;
                }
                break;
            case "bersek":
                if (money >= 100)
                {
                    console.log("bersek created");
                    gridMap[m_mouseCurrentYTile][m_mouseCurrentXTile] = 2;
                    // temp bullet
                    var newBersekBullet = new bulletObject(m_bersekTowerBulletImage, 0, 0, 50, 50, false, 0);
                    // temp bersek
                    var newBersek = new GameObject(m_bersekTowerImage, "bersek", 
                    m_mouseCurrentXTile * s_gridWidth, m_mouseCurrentYTile * s_gridHeight,
                    m_bersekTowerWidth, m_bersekTowerHeight, 0, m_mouseCurrentXTile, m_mouseCurrentYTile,
                    "None", [0, 1, 2, 3], 7, 0, 0, 0, newBersekBullet, 0);
                    // push temp bersek back
                    m_bersekArray.push(newBersek);
                    money -= 100;
                }
                break;
            default:
                break;
        }
    }
}
// Zombie spritesheet atlas references
// row 0 up
// row 1 down
// row 2 right
// row 3 left
// movement update
console.log("movement.js detected");

function borderBoundCollision(GameObject)
{
    if (GameObject.xpos <= 0) // collide with left edge
    {
        GameObject.xpos = 0;
    }
    else if (GameObject.xpos >= canvas.width - GameObject.width) // collide with right edge
    {
        GameObject.xpos = canvas.width - GameObject.width
    }
    if (GameObject.ypos <= 0) // collide with top edge
    {
        GameObject.ypos = 0;
    }
    else if (GameObject.ypos >= canvas.height - GameObject.height - (canvas.height - (s_gridMapHeightNum) * s_gridHeight)) // collide with bottom edge
    {
        GameObject.ypos = canvas.height - GameObject.height - (canvas.height - (s_gridMapHeightNum) * s_gridHeight);
    }
}

// Zombie spritesheet atlas references
// row 0 up
// row 1 down
// row 2 right
// row 3 left
function zombieMovementUpdate(GameObject) {
    if (GameObject.gamerInput.action === "Up") {
        if (GameObject.currentYTile > 0 &&
            gridMap[GameObject.currentYTile - 1][GameObject.currentXTile] !== 0) // wall at top 
        {
            GameObject.currentDirection = 0;
        }
        else 
        {
            GameObject.ypos -= GameObject.speed; // Move Player Up
            GameObject.currentDirection = 0;
        }
    }
    else if (GameObject.gamerInput.action === "Down") {
        if (GameObject.currentYTile < (s_gridMapHeightNum - 1) &&
            gridMap[GameObject.currentYTile + 1][GameObject.currentXTile] !== 0) // wall at below
        {
            GameObject.currentDirection = 1;
        }
        else 
        {
            GameObject.ypos += GameObject.speed; // Move Player Down
            GameObject.currentDirection = 1;
        }
    }
    else if (GameObject.gamerInput.action === "Left") {
        if (GameObject.currentXTile > 0 &&
            gridMap[GameObject.currentYTile][GameObject.currentXTile - 1] !== 0) // wall on left
        {
            GameObject.currentDirection = 3;
        }
        else 
        {
            GameObject.xpos -= GameObject.speed; // Move Player Left
            GameObject.currentDirection = 3;
        }
    }
    else if (GameObject.gamerInput.action === "Right") {
        if (GameObject.currentXTile < (s_gridMapWidthNum - 1) &&
            gridMap[GameObject.currentYTile][GameObject.currentXTile + 1] !== 0) // wall on right
        {
            GameObject.currentDirection = 2;
        }
        else 
        {
            GameObject.xpos += GameObject.speed; // Move Player Right
            GameObject.currentDirection = 2;
        }
    }
    else if (GameObject.gamerInput.action === "Left Up") {
        if (GameObject.currentYTile > 0 &&
            gridMap[GameObject.currentYTile - 1][GameObject.currentXTile] !== 0) // wall at top
        {
            GameObject.xpos -= GameObject.speed;
            GameObject.currentDirection = 3;
        }
        else if (GameObject.currentXTile > 0 &&
            gridMap[GameObject.currentYTile][GameObject.currentXTile - 1] !== 0) // wall on left
        {
            GameObject.ypos -= GameObject.speed;
            GameObject.currentDirection = 0;
        }
        else 
        {
            GameObject.xpos -= GameObject.speed;
            GameObject.ypos -= GameObject.speed;
            GameObject.currentDirection = 3;
        }
    }
    else if (GameObject.gamerInput.action === "Left Down") {
        if (GameObject.currentYTile < (s_gridMapHeightNum - 1) &&
            gridMap[GameObject.currentYTile + 1][GameObject.currentXTile] !== 0) // wall at below
        {
            GameObject.xpos -= GameObject.speed;
            GameObject.currentDirection = 3;
        }
        else if (GameObject.currentXTile > 0 &&
            gridMap[GameObject.currentYTile][GameObject.currentXTile - 1] !== 0) // wall on left
        {
            GameObject.ypos += GameObject.speed;
            GameObject.currentDirection = 1;
        }
        else 
        {
            GameObject.xpos -= GameObject.speed;
            GameObject.ypos += GameObject.speed;
            GameObject.currentDirection = 3;
        }
    }
    else if (GameObject.gamerInput.action === "Right Up") {
        if (GameObject.currentXTile < (s_gridMapWidthNum - 1) &&
            gridMap[GameObject.currentYTile][GameObject.currentXTile + 1] !== 0) // wall on right
        {
            GameObject.ypos -= GameObject.speed;
            GameObject.currentDirection = 0;
        }
        else if (GameObject.currentYTile > 0 &&
            gridMap[GameObject.currentYTile - 1][GameObject.currentXTile] !== 0) // wall at top
        {
            GameObject.xpos += GameObject.speed;
            GameObject.currentDirection = 2;
        }
        else 
        {
            GameObject.xpos += GameObject.speed;
            GameObject.ypos -= GameObject.speed;
            GameObject.currentDirection = 2;
        }
    }
    else if (GameObject.gamerInput.action === "Right Down") {
        if (GameObject.currentYTile < (s_gridMapHeightNum - 1) &&
            gridMap[GameObject.currentYTile + 1][GameObject.currentXTile] !== 0) // wall at below
        {
            GameObject.xpos += GameObject.speed;
            GameObject.currentDirection = 2;
        }
        else if (GameObject.currentXTile < s_gridMapWidthNum &&
            gridMap[GameObject.currentYTile][GameObject.currentXTile + 1] !== 0) // wall on right
        {
            GameObject.ypos += GameObject.speed;
            GameObject.currentDirection = 1;
        }
        else 
        {
            GameObject.xpos += GameObject.speed;
            GameObject.ypos += GameObject.speed;
            GameObject.currentDirection = 2;
        }
    }
}

// Player spritesheet atlas references
// row 0 left
// row 1 right
function playerMovementUpdate(GameObject)
{
    if (GameObject.gamerInput.action === "Up") {
        if (GameObject.currentYTile > 0 &&
            gridMap[GameObject.currentYTile - 1][GameObject.currentXTile] != 0) // wall at top 
        {
            GameObject.currentDirection = GameObject.currentDirection;
        }
        else {
            GameObject.ypos -= GameObject.speed; // Move Player Up
            GameObject.currentDirection = GameObject.currentDirection;
        }
    }
    else if (GameObject.gamerInput.action === "Down") {
        if (GameObject.currentYTile < (s_gridMapHeightNum - 1) &&
            gridMap[GameObject.currentYTile + 1][GameObject.currentXTile] != 0) // wall at below
        {
            GameObject.currentDirection = GameObject.currentDirection;
        }
        else {
            GameObject.ypos += GameObject.speed; // Move Player Down
            GameObject.currentDirection = GameObject.currentDirection;
        }
    }
    else if (GameObject.gamerInput.action === "Left") {
        if (GameObject.currentXTile > 0 &&
            gridMap[GameObject.currentYTile][GameObject.currentXTile - 1] != 0) // wall on left
        {
            GameObject.currentDirection = 0;
        }
        else {
            GameObject.xpos -= GameObject.speed; // Move Player Left
            GameObject.currentDirection = 0;
        }
    }
    else if (GameObject.gamerInput.action === "Right") {
        if (GameObject.currentXTile < (s_gridMapWidthNum - 1) &&
            gridMap[GameObject.currentYTile][GameObject.currentXTile + 1] != 0) // wall on right
        {
            GameObject.currentDirection = 1;
        }
        else {
            GameObject.xpos += GameObject.speed; // Move Player Right
            GameObject.currentDirection = 1;
        }
    }
}
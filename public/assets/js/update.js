// attack function
function attack(towerGameObject, enemyGameObject)
{
    // bersek attack
    if (towerGameObject.attribute === "bersek")
    {
        // search 8 grids around itself
        if (towerGameObject.currentXTile > 0 && towerGameObject.currentYTile > 0)
        {
            if ((enemyGameObject.currentXTile === towerGameObject.currentXTile
                || enemyGameObject.currentXTile === (towerGameObject.currentXTile + 1)
                || enemyGameObject.currentXTile === (towerGameObject.currentXTile - 1))
                &&
                (enemyGameObject.currentYTile === towerGameObject.currentYTile
                || enemyGameObject.currentYTile === (towerGameObject.currentYTile + 1)
                || enemyGameObject.currentYTile === (towerGameObject.currentYTile - 1)))
            {
                // update bullet status
                if (!towerGameObject.bullet.canDraw)
                {
                    towerGameObject.bullet.canDraw = true;
                    towerGameObject.bullet.xpos = enemyGameObject.xpos;
                    towerGameObject.bullet.ypos = enemyGameObject.ypos;
                    enemyGameObject.health -= 10;
                    m_attackSound.play();
                }
            }
        }
    }
}

// attack function for bersek
function bersekAttack()
{
    for (let j = 0; j < m_zombieArray.length; j++)
    {
        for (var i = 0; i < m_bersekArray.length; i++)
        {
            if (m_zombieRespawn[j])
            {
                attack(m_bersekArray[i], m_zombieArray[j]);
            }
        }
    }
}

// get input for player only
function input(event) 
{
    if (event.type === "keydown") 
    {
        //console.log(event);
        switch (event.keyCode) 
        {
            case 37: // Left Arrow
                player.gamerInput = new GamerInput("Left");
                break;
            case 38: // Up Arrow
                player.gamerInput = new GamerInput("Up");
                break;
            case 39: // Right Arrow
                player.gamerInput = new GamerInput("Right");
                break;
            case 40: // Down Arrow
                player.gamerInput = new GamerInput("Down");
                break;
            default:
                player.gamerInput = new GamerInput("None"); //No Input
                break;
        }
    }
    else if (event.type === "mousedown" || event.type === "touchstart")
    {
        //console.log(event);
        if (event.target.className === "button W round")
        {
            player.gamerInput = new GamerInput("Up");
        }
        else if (event.target.className === "button S round")
        {
            player.gamerInput = new GamerInput("Down");
        }
        else if (event.target.className === "button A round")
        {
            player.gamerInput = new GamerInput("Left");
        }
        else if (event.target.className === "button D round")
        {
            player.gamerInput = new GamerInput("Right");
        }
        // create wall & towers
        if (m_mouseCurrentXTile === Math.floor(m_wallToolbar.xpos / s_gridWidth) && m_mouseCurrentYTile >= 10
        && !m_wallToolbar.isChoosen)
        {
            console.log("wall choosen");
            m_wallToolbar.isChoosen = true;
            m_bersekToolbar.isChoosen = false;
        }
        else if (m_wallToolbar.isChoosen)
        {
            create(m_wallToolbar,event);
        }
        // bersek tower
        if (m_mouseCurrentXTile === Math.floor(m_bersekToolbar.xpos / s_gridWidth) && m_mouseCurrentYTile >= 10
        && !m_bersekToolbar.isChoosen)
        {
            console.log("bersek choosen");
            m_wallToolbar.isChoosen = false;
            m_bersekToolbar.isChoosen = true;
        }
        else if (m_bersekToolbar.isChoosen)
        {
            create(m_bersekToolbar,event);
        }
    }
    else 
    {
        player.gamerInput = new GamerInput("None");
    }
}

function increaseMoney()
{
    if (m_localStorageChecked)
    {
        m_moneyTimeCounter++;
        if (m_moneyTimeCounter > moneyTimeLimit)
        {
            money += 10;
            m_moneyTimeCounter = 0;
        }
    }
}

function zombieArrayUpdate()
{
    for (let i = 0; i < m_zombieArray.length; i++)
    {
        if (m_zombieRespawn[i])
        {
            tileUpdate(m_zombieArray[i]);
            zombieMovementUpdate(m_zombieArray[i]);
            borderBoundCollision(m_zombieArray[i]);
            enemyFindPath(m_zombieArray[i].currentXTile, m_zombieArray[i].currentYTile, 19, 9, m_zombieArray[i]);
            if (m_zombieArray[i].health <= 0 && m_zombieRespawn[i])
            {
                m_zombieRespawn[i] = false;
                m_deadZombieCounter++;
                m_zombieDeadSound.play();
            }
        }
    }
    if (m_deadZombieCounter === 5)
    {
        m_lose = false;
        m_win = true;
    }
}

function setLocalStorage()
{
    // JSON.stringify
    if (m_localStorageChecked)
    {
        localStorage.setItem("gridMap", gridMap);
        localStorage.setItem("zombie", m_zombieArray);
        localStorage.setItem("money", money);
    }
}

function update(event) 
{
    //console.log(gridMap);
    m_backgroundSound.play();
    responsiveCanvasChange();
    checkScoreDetector();
    zombieSpawn();
    zombieArrayUpdate();
    increaseMoney();
    //playerMovementUpdate(player);
    borderBoundCollision(player);
    tileUpdate(player);
    bersekAttack();
    //mouseTileUpdate(event)
    //setLocalStorage();
}